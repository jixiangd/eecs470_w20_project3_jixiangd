gui_state_default_create -off -ini

# Globals
gui_set_state_value -category Globals -key recent_sessions -value {{gui_load_session -ignore_errors -file {/afs/umich.edu/user/j/i/jixiangd/Documents/lab1/view01.tcl .tcl}}}

# Layout
gui_set_state_value -category Layout -key child_console_size_x -value 1535
gui_set_state_value -category Layout -key child_console_size_y -value 105
gui_set_state_value -category Layout -key child_data_coltype -value 91
gui_set_state_value -category Layout -key child_data_colvalue -value 148
gui_set_state_value -category Layout -key child_data_colvariable -value 188
gui_set_state_value -category Layout -key child_data_size_x -value 356
gui_set_state_value -category Layout -key child_data_size_y -value 493
gui_set_state_value -category Layout -key child_driver_size_x -value 1263
gui_set_state_value -category Layout -key child_driver_size_y -value 179
gui_set_state_value -category Layout -key child_hier_col1 -value 1
gui_set_state_value -category Layout -key child_hier_col2 -value 0
gui_set_state_value -category Layout -key child_hier_col3 -value {-1}
gui_set_state_value -category Layout -key child_hier_colhier -value 183
gui_set_state_value -category Layout -key child_hier_colpd -value 0
gui_set_state_value -category Layout -key child_hier_coltype -value 120
gui_set_state_value -category Layout -key child_hier_size_x -value 232
gui_set_state_value -category Layout -key child_hier_size_y -value 493
gui_set_state_value -category Layout -key child_schematic_docknewline -value false
gui_set_state_value -category Layout -key child_schematic_pos_x -value {-2}
gui_set_state_value -category Layout -key child_schematic_pos_y -value {-15}
gui_set_state_value -category Layout -key child_schematic_size_x -value 788
gui_set_state_value -category Layout -key child_schematic_size_y -value 414
gui_set_state_value -category Layout -key child_source_docknewline -value false
gui_set_state_value -category Layout -key child_source_pos_x -value {-2}
gui_set_state_value -category Layout -key child_source_pos_y -value {-15}
gui_set_state_value -category Layout -key child_source_size_x -value 950
gui_set_state_value -category Layout -key child_source_size_y -value 488
gui_set_state_value -category Layout -key child_wave_colname -value 230
gui_set_state_value -category Layout -key child_wave_colvalue -value 211
gui_set_state_value -category Layout -key child_wave_left -value 445
gui_set_state_value -category Layout -key child_wave_right -value 1085
gui_set_state_value -category Layout -key main_pos_x -value 1
gui_set_state_value -category Layout -key main_pos_y -value 38
gui_set_state_value -category Layout -key main_size_x -value 1536
gui_set_state_value -category Layout -key main_size_y -value 735
gui_set_state_value -category Layout -key stand_wave_child_docknewline -value false
gui_set_state_value -category Layout -key stand_wave_child_pos_x -value {-2}
gui_set_state_value -category Layout -key stand_wave_child_pos_y -value {-15}
gui_set_state_value -category Layout -key stand_wave_child_size_x -value 1540
gui_set_state_value -category Layout -key stand_wave_child_size_y -value 593
gui_set_state_value -category Layout -key stand_wave_top_pos_x -value 60
gui_set_state_value -category Layout -key stand_wave_top_pos_y -value 66
gui_set_state_value -category Layout -key stand_wave_top_size_x -value 1595
gui_set_state_value -category Layout -key stand_wave_top_size_y -value 763

# list_value_column

# Sim

# Assertion

# Stream

# Data

# TBGUI

# Driver

# Class

# Member

# ObjectBrowser

# UVM

# Local

# Backtrace

# FastSearch

# Exclusion

# SaveSession

# FindDialog
gui_create_state_key -category FindDialog -key m_pMatchCase -value_type bool -value false
gui_create_state_key -category FindDialog -key m_pMatchWord -value_type bool -value false
gui_create_state_key -category FindDialog -key m_pUseCombo -value_type string -value {}
gui_create_state_key -category FindDialog -key m_pWrapAround -value_type bool -value true


gui_state_default_create -off
