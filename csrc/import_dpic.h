
 extern void print_header(/* INPUT */const char* str);

 extern void print_cycles();

 extern void print_stage(/* INPUT */const char* div, /* INPUT */int inst, /* INPUT */int npc, /* INPUT */int valid_inst);

 extern void print_reg(/* INPUT */int wb_reg_wr_data_out_hi, /* INPUT */int wb_reg_wr_data_out_lo, /* INPUT */int wb_reg_wr_idx_out, /* INPUT */int wb_reg_wr_en_out);

 extern void print_membus(/* INPUT */int proc2mem_command, /* INPUT */int mem2proc_response, /* INPUT */int proc2mem_addr_hi, /* INPUT */int proc2mem_addr_lo, /* INPUT */int proc2mem_data_hi, /* INPUT */int proc2mem_data_lo);

 extern void print_close();
